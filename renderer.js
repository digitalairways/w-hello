(function() {

    var factory = function (gibson) {

        gibson.export.renderer = {
            html: function(token) {
                return "<b>Hello "+ token.data.value +"</b><br>";
            }
        };

        gibson.export.listeners = function() {
        };

    };

    factory(window.gibson)

})();

(function() {

    var factory = function (gibson) {

        gibson.export.tokenizer = function(token) {
            var regexs = [
                /^w-hello?:(.+\n)*\n/ //markup
            ];
            return WidgetBuilder.tokenizer(regexs, token);
        };

        gibson.export.token = function(name, res, src) {
            var token = { type: "widget", name: name };
            var data = {};
            data.value = res[1];
            token.data = data;
            return token;
        };
    };

    factory(window.gibson)

})();
